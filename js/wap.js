//2024
console.log($);
$(document).ready(function(e) {
	var size = 1,
		width = 1000,
		vertical = true,
		cross = false;
	var direction = function() {
		var docEl = document.documentElement || document.body;
		setTimeout(function() {
			var clientWidth = docEl.clientWidth;
			var clientHeight = docEl.clientHeight;
			if(clientWidth > clientHeight) {
				$('html').addClass('cross').removeClass('vertical');
				cross = true;
				vertical = false;
			} else {
				$('html').addClass('vertical').removeClass('cross');
				cross = false;
				vertical = true;
			}
			recalc()
		}, 200)

	}
	direction();

	var recalc = function() {
		var docEl = document.documentElement || document.body;
		var clientWidth = docEl.clientWidth;
		var clientHeight = docEl.clientHeight;

		if(!clientWidth) return;
		if(vertical) {
			docEl.style.fontSize = 100 * (clientWidth / 320) + 'px';
			size = clientWidth / 320;
		} else {
			docEl.style.fontSize = 100 * (clientHeight / 320) + 'px';
			size = clientWidth / 320;
		}

	};
	var _hmt = _hmt || [];
	(function() {
		var hm = document.createElement("script");
		hm.src = "https://hm.baidu.com/hm.js?d924028c274d650b6f12d391ec251143";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(hm, s);
	})();
	(function() {
		var bp = document.createElement('script');
		var curProtocol = window.location.protocol.split(':')[0];
		if(curProtocol === 'https') {
			bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
		} else {
			bp.src = 'http://push.zhanzhang.baidu.com/push.js';
		}
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(bp, s);
	})();
	var docEl = document.documentElement || document.body;
	var clientWidth = docEl.clientWidth;
	var clientHeight = docEl.clientHeight;
	var mySwiper = new Swiper ('#swiper1', {
	    direction: 'vertical',
	    loop: true,
	     hashNavigation: true,
	     on: {
		    slideChange: function () {
		    	var index = this.activeIndex-1;
		    	if(index>3){
		    		index = 0
		    	}
		      $(".nav li").eq(index).addClass("active").siblings().removeClass("active");
		    }
		  }
	  })  


	window.addEventListener("orientationchange", direction, false);
	var html = function(data) {
		var li = "";
		$.each(data, function(i, index) {
			if(i < 12) {
				li += '<li data-url="' + index.url + '" data-name="' + index.name + '" class="js_cell_case">' +
					'<span class="zz"></span>' +
					'<img src="' + index.img + '" />' +
					'<p class="case_info">' +
					'<span>' + index.title + '</span>' +
					//					'<span>' + index.text + '</span>' +
					'</p>' +
					'</li>'
			}
		})
		return li;
	}
	$.getJSON("js/case.json", function(data) {
		$('#js_wrap_case .list_case').eq(0).html(html(data[0]));
		$('#js_wrap_case .list_case').eq(1).html(html(data[1]));
		$('#js_wrap_case .list_case').eq(2).html(html(data[2]));
		$('#js_wrap_case .list_case').eq(3).html(html(data[3]));
		var mySwiper = new Swiper ('#swiper2', {
		    direction: 'horizontal',
		    loop: true,
		    
		    // 如果需要分页器
		    pagination: {
		      el: '.swiper-pagination',
		    },
		    // 如果需要前进后退按钮
		    navigation: {
		      nextEl: '.swiper-button-next',
		      prevEl: '.swiper-button-prev',
		    }
		  }) 
	})

	$(".nav li").on('click', function() {
		var self = $(this);
		var menuanchor = self.data("menuanchor");
		self.addClass("active").siblings().removeClass("active");
		mySwiper.slideTo(menuanchor, 300, false);
	})
	$(".list_case").on('click', "span", function() {
		var self = $(this).parents("li");
		var url = self.data("url");
		window.location.href = url;
		console.log(url);
	})

});